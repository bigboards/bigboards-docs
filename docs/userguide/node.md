# BigBoards Node
A hex is build around 6 nodes. Each of these nodes provides processing and storage capabilities for working and experimenting with distributed technologies.

## Specifications

| ------------- | ------------------------------------------------- |
| **CPU**       | Freescale i.MX6 Quad Cortex-A9                    |
| **memory**    | 2048 Mb DDR3                                      |
| **GPU**       | Vivante GC 2000 + Vivante GC 355 + Vivante GC 320 |
| **network**   | Gigabit Ethernet currently capped at ~480MB/s     |
| **storage**   | 1Tb 5400 RPM SATA II                              |

The network on the boards we are using are capped to ~480MB/s by the i.MX6 DMA.